Our mission is to provide exceptional care for the hearing impaired and those suffering with tinnitus by taking the time to educate patients and work together to find the best solution for each individual to make hearing and their life more enjoyable.

Address: 3139 Bluestem Dr, Suite 108, West Fargo, ND 58078, USA

Phone: 701-639-4595